@extends('welcome')
@push('css-scripts')
 
@endpush
@section('content')
    <div class="container">
        <div class="d-flex">
            <div class="mr-4">
                <h2>Daftar Produk</h2>
            </div>
            <div class="">
                <a href="{{ route('produk.create') }}" class="btn btn-primary"><span class="fa fa-plus"></span></a>
            </div>
            <div class="ml-auto">
                <div class="input-group">
                    <input type="text" class="form-control" id="search" placeholder="Search">
                    <div class="input-group-append">
                      <a href="javascript:;" class="btn btn-primary" id="btn-search"><span class="fa fa-search"></span></a>
                    </div>
                  </div>
            </div>
        </div>
        @if (session('message'))
            <div class="alert alert-success">{{ session('message') }}</div>
        @endif
        <div class="d-flex flex-wrap my-row">
            <table class="table">
                <thead>
                  <tr>
                    <th>Nama Barang</th>
                    <th>Kode Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Tanggal</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($produk as $p)
                        <tr>
                            <td>{{ $p->nama_barang }}</td>
                            <td>{{ $p->kode_barang }}</td>
                            <td>{{ $p->jumlah_barang }}</td>
                            <td>{{ $p->tanggal }}</td>
                            <td>
                                <a href="{{ route('produk.edit', $p->id) }}" class="btn btn-success">Edit</a>
                                <a href="{{ route('produk.delete', $p->id) }}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>Barang Kosong</td>
                        </tr>
                    @endforelse
                </tbody>
              </table>
        </div>
@endsection

@push('js-scripts')
    <script>
        $(document).ready(function() {
            $('#btn-search').click(function() {
                var keyword = $('#search').val();
                if(keyword){
                    var url = "{{url('/produk/search')}}" + '/' + keyword;
                    console.log(url);
                    window.location.href = url;
                }
            })
        })
    </script>
@endpush
