<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('produk.index');
});

Route::prefix('produk')->group(function () {
    Route::get('/', [App\Http\Controllers\ProdukController::class, 'index'])->name('produk.index');
    Route::get('/search/{keyword}', [App\Http\Controllers\ProdukController::class, 'search'])->name('produk.search');
    Route::get('/create', [App\Http\Controllers\ProdukController::class, 'create'])->name('produk.create');
    Route::post('/store', [App\Http\Controllers\ProdukController::class, 'store'])->name('produk.store');
    Route::get('/{produk}/edit', [App\Http\Controllers\ProdukController::class, 'edit'])->name('produk.edit');
    Route::post('/{produk}/update', [App\Http\Controllers\ProdukController::class, 'update'])->name('produk.update');
    Route::get('/{produk}/delete', [App\Http\Controllers\ProdukController::class, 'destroy'])->name('produk.delete');
});
