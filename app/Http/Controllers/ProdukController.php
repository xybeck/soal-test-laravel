<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;

class ProdukController extends Controller
{
    //

    public function index() 
    {
        $data['produk'] = Produk::all();

        return view('produk.index', $data);
    }

    public function create()
    {
        return view('produk.create');
    }
    
    public function store(Request $request)
    {
        $validate = $request->validate([
            'nama_barang' => 'required',
            'kode_barang' => 'required',
            'jumlah_barang' => 'required|integer',
            'tanggal' => 'required|date',
        ], [
            'required' => ':attribute harus diisi',
            'integer' => ':attribute harus diisi dengan angka bulat',
            'date' => 'harus diisi dengan format tanggal'
        ]);
        $data = $request->all();
        
        Produk::create($data);
        
        return redirect()->route('produk.index')->with('message', 'Data telah ditambahkan');
    }

    public function edit($id)
    {
        $data['produk'] = Produk::find($id);

        return view('produk.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'nama_barang' => 'required',
            'kode_barang' => 'required',
            'jumlah_barang' => 'required|integer',
            'tanggal' => 'required|date',
        ], [
            'required' => ':attribute harus diisi',
            'integer' => ':attribute harus diisi dengan angka bulat',
            'date' => 'harus diisi dengan format tanggal'
        ]);
        
        $data = $request->all();

        $produk = Produk::find($id);

        $produk->update($data);

        return redirect()->route('produk.index')->with('message', 'Data telah diubah');
    }

    public function destroy($id)
    {
        $produk = Produk::find($id);

        $produk->delete();

        return redirect()->route('produk.index')->with('message', 'Data telah dihapus');
    }

    public function search($keyword)
    {
        $data['produk'] = Produk::where('nama_barang','like',"%$keyword%")->orWhere('kode_barang','like',"%$keyword%")->get();
        $data['keyword'] = $keyword;
        return view('produk.search', $data);
    }
}
