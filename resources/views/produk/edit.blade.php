@extends('welcome')
@push('css-scripts')
<style>
    .error {
        color: red;
    }
</style>
@endpush
@section('content')
    <div class="container">
        <div class="d-flex">
            <div class="mr-4">
                <h2>Update Produk</h2>
            </div>
        </div>
        <br>
        @if (session('message'))
            <div class="alert alert-success">{{ session('message') }}</div>
        @endif
        <div>
            <form action="{{ route('produk.update', $produk->id)}}" method="post">
                @csrf
                <div class="form-group row">
                  <label for="nama_barang" class="col-sm-2 col-form-label">Nama Barang</label>
                  <div class="col-sm-10">
                      <input type="text" name="nama_barang" class="form-control" id="nama_barang" placeholder="Nama Barang" value="{{ old('nama_barang') ? old('nama_barang') : $produk->nama_barang }}">
                      @error('nama_barang')
                            <span class="error">*{{ $message }}</span>
                        @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="kode_barang" class="col-sm-2 col-form-label">Kode Barang</label>
                  <div class="col-sm-10">
                      <input type="text" name="kode_barang" class="form-control" id="kode_barang" placeholder="Kode Barang" value="{{ old('kode_barang') ? old('kode_barang') : $produk->kode_barang }}">
                      @error('kode_barang')
                            <span class="error">*{{ $message }}</span>
                        @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="jumlah_barang" class="col-sm-2 col-form-label">Jumlah Barang</label>
                  <div class="col-sm-10">
                      <input type="text" name="jumlah_barang" class="form-control" id="jumlah_barang" placeholder="Jumlah Barang" value="{{ old('jumlah_barang') ? old('jumlah_barang') : $produk->jumlah_barang }}">
                      @error('jumlah_barang')
                            <span class="error">*{{ $message }}</span>
                        @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="tanggal" class="col-sm-2 col-form-label">Tanggal</label>
                  <div class="col-sm-10">
                      <input type="date" name="tanggal" class="form-control" id="tanggal" placeholder="Tanggal" value="{{ old('tanggal') ? old('tanggal') : $produk->tanggal }}">
                      @error('tanggal')
                            <span class="error">*{{ $message }}</span>
                        @enderror
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
@endsection

@push('js-scripts')

@endpush
